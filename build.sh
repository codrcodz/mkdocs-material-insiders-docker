#!/usr/bin/env bash

main() {
  echo -e "\n[INFO] Logging into project's container registry (${CI_REGISTRY})."
  docker login \
    -u "${CI_REGISTRY_USER}" \
    -p "${CI_REGISTRY_PASSWORD}" \
    "${CI_REGISTRY}" || return 1
  echo -e "\n[INFO] Building image (${CI_REGISTRY_IMAGE}/image:mr-${CI_MERGE_REQUEST_IID})."
  docker build \
    --pull \
    -t "${CI_REGISTRY_IMAGE}/image:mr-${CI_MERGE_REQUEST_IID}" \
    --target "image" \
    -f Dockerfile \
    --build-arg "GH_TOKEN=${GH_TOKEN}" \
    . || return 1
  echo -e "\n[INFO] Pushing image (${CI_REGISTRY_IMAGE}/image:mr-${CI_MERGE_REQUEST_IID})."
  docker push "${CI_REGISTRY_IMAGE}/image:mr-${CI_MERGE_REQUEST_IID}" || return 1
}

main || exit 1
