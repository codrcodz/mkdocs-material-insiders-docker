FROM docker.io/python:3-alpine as image
ARG GH_TOKEN=$GH_TOKEN
COPY ./entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
RUN apk add --no-cache \
      bash git openssl \
      build-base cairo cairo-dev cairo-tools \
      jpeg-dev zlib-dev freetype-dev lcms2-dev openjpeg-dev tiff-dev tk-dev tcl-dev
RUN pip install --disable-pip-version-check --root-user-action=ignore \
      git+https://$GH_TOKEN@github.com/squidfunk/mkdocs-material-insiders.git \
      cairosvg \
      pillow \
      pngquant
ENTRYPOINT /entrypoint.sh
