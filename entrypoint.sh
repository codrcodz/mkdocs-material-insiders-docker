#!/usr/bin/env bash

entrypointMain() {

  local copyright
  local copyright_holder
  local timestamp
  local origin

  copyright_holder="${1:-$COPYRIGHT_HOLDER}"
  copyright="Copyright &copy; ${copyright_holder}"
  hash date || return 1
  timestamp="Last Site Update: $(date -Iseconds)"
  if [[ "${GITLAB_CI}" == "true" ]]; then
    origin="Origin Server: Gitlab Pages"
  elif [[ "${GITHUB_ACTIONS}" == "true" ]]; then
    origin="Origin Server: Github Pages"
  else
    origin="Origin Server: ${HOSTNAME}"
  fi

  # This string replaces the "copyright:" line in the config file at website build time
  copyright_string="copyright: \"${copyright}<br>${timestamp}<br>${origin}\""
  echo "[INFO] Replacing previous copyright line in mkdocs.yml with:"
  echo "       ${copyright_string}"

  # This loop is basically a pure bash replacement for sed's string substitution operation
  while IFS=$'\n' read -r line; do
    if [[ "${line}" =~ ^copyright: ]]; then
      config_lines+=( "${copyright_string}" )
    else
      config_lines+=( "${line}" )
    fi
  done < ./mkdocs.yml

  : > ./mkdocs.yml
  for config_line in "${config_lines[@]}"; do
    echo "${config_line}" >> ./mkdocs.yml
  done

}

entrypointMain "$1" || exit 1
